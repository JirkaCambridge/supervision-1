package sVW1;
public class JirkaSVW1 {
	/*Q1:
	 * 		Declarative Programming										Imperative programming
	 * - Declare what you want not how you want it done				-Exactly specify instructions
	 * (i.e. the compiler can also change your code to				and the compiler/interpreter
	 * something equivalent yet different)							will then perform EXACTLY that.
	 * (This is called optimising)
	 * 
	 * 
	 * 
	 * -Data is predominantly immutable								-Data is predominantly mutable
	 * 
	 * 
	 * 
	 * -Aims to use functions and pass values						-Uses imperative statements to 			
	 * as arguments													directly manipulate (change) state
	 * 
	 * 
	 * 
	 * -Code usually short and concise								-Long code & more effort required
	 * 																to make it nice and avoid bugs
	 * 
	 * 
	 * -...but can be harder to get your head around				-...but directly changing machine
	 * and slower													state allows the programmer
	 * 																to optimise his code better.
	 * */
	//Q2 - primitives - boolean, char, byte, short, int, long, float, double and void.
	//i.e.
	public int k = 10;
	public char myChar = 'a';
	//etc.
	/*References are pointing to the memory addresses rather than to the actual values. Everything
	 * apart from primitives is a reference type in java.
	 */ 
	int [] aa = new int [] {1,2};
	//classes
	public class Example {
		private int exampleInt = 5;
		public void printMyInt ()	{
			System.out.println (exampleInt);
		}
	}
	//objects: the code bellow creates a reference to an int array object. 
	int [] aaa = new int [] {1,2,3};

	/*
	 * 3:
	 * 1. 
	 * p->Null
	 * 
	 * 2.
	 * p->Null
	 * p2->Person X
	 * 
	 * 3.
	 * p2->Person X
	 * p->Person X
	 * 
	 * 4.
	 * p2->Person Y
	 * p-> Person X
	 * 
	 * 5.
	 * p2-> Person Y
	 * p-> Null
	 * */

	int sum(int [] a)	{
		//produces the sum of the elements of a
		int result = 0;
		for (int i=0;i<a.length;i++)	{
			result+=a[i];
		}
		return result;
	}
	int [] cumSum (int [] a)	{
		//returns the cumulative sum of a
		int [] result = new int [a.length];
		int cur = 0;
		for (int i=0;i<a.length;i++){
			cur+=a[i];
			result[i]=cur;
		}
		return result;
	}
	int [] positives (int [] a)	{
		//returns an array of positive numbers in a
		//first find the length of the new array
		int newLength = 0;
		for (int i=0;i<a.length;i++)	{
			if (a[i]>0)	{
				newLength++;
			}
		}
		//no create the new array and fill it in and return
		int [] result = new int [newLength];
		int posRes = 0; //keeps track of our position in result array
		for (int i=0;i<a.length;i++)	{
			if (a[i]>0)	{
				result[posRes]=a[i];
				posRes++;
			}
		}
		return result;
	}
	/*5. I am a bit confused about what the function should take as an input, but in the end I
	 * decided that the function should take number n as an input and produce an empty nXn matrix
	 */
	float [] [] matrixMaker (int n)	{
		return new float [n] [n];
	}
	/*6. It doesn't work because the variables that the function creates only exist within the
	 * scope of the function (i.e. Java passes by value not by reference so that when the
	 * function takes the arguments, it makes a copy of them and the original ones remain intact).
	 * 
	 * Now coming to a question of how to do this without using a custom class, I thought it is
	 * impossible for a java function to just change the values of a general variable in the
	 * program? One way of doing this would be having the addVector function return (x, y),
	 * but that would require us to modify the code in main which I'm not sure if we're allowed to
	 * do? A proper OOP approach would be to make a vector class I think, but then we'd still
	 * have to change the code inside the main function a little bit?
	 * 
	 * Since we should not make a class, I will implement the first option:
	 */
	public static int[] vectorAdd (int x, int y, int dx, int dy)	{
		int[] result = {x+dx,y+dy};
		return result;
	}
	/*This also requires changes to the main method which are implemented in the main method
	 * (bellow).
	 */
	public static void main(String[] args) { 
		int a=0; 
		int b=2; 
		int [] result = vectorAdd(a,b,1,1);
		a=result [0];
		b=result [1];
		System.out.println(a+" "+b); 
		// (a,b) is still (0,2) 
	}

	/*7. Continues in file SLList, which is aided by Node.java*/

}

