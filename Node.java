package sVW1;
/*7. Design and implement (from scratch � i.e. not using java lists) a singly linked list. Your 
class(es) should support the addition and removal of elements, querying of the head 
element, obtaining the nth element and computing the length of the list. Your list only needs 
to work with integers. Do not use Generics. 

Class node that is used for particular nodes in the list is declared here*/
public class Node {
	private Node next; //the next node (i. e. the one this one links to)
	private int content; //the node has some content
	public Node (int label)	{
		//Initialises the last node
		content = label;
		next = null;
	}
	public Node (int label, Node goTo)	{
		//Initialises any other node
		content = label;
		next = goTo;
	}
	public void setCon (int x){
		//Sets the node's label
		content = x;
	}
	public void setNext (Node x)	{
		//Sets where the node links to
		next = x;
	}
	public int getCon (){
		//Gets the node's label
		return content;
	}
	public Node getNext ()	{
		//Gets where the node links to
		return next;
	}
}
