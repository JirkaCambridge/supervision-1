package sVW1;
/*7. Design and implement (from scratch � i.e. not using java lists) a singly linked list. Your 
class(es) should support the addition and removal of elements, querying of the head 
element, obtaining the nth element and computing the length of the list. Your list only needs 
to work with integers. Do not use Generics. 

The class SLList which links particular nodes together is declared here*/
public class SLList {
	private Node head; //Throughout the links from the first node, we can get anywhere
	private int size;
	public SLList ()	{
		//An empty list to start with
		head = null;
		size = 0;
	}
	public Node getHead ()	{
		return head;
	}
	public int getSize ()	{
		return size;
	}
	public int getElement (int index)	{
		Node currNode = head;
		if (!(index==0)){
			for (int i = 0; i<index; i++)	{
				currNode=currNode.getNext();
				/*traversing the tree to the node we want to get. This works because
				 * we start with currNode being at position 0, then everytime we run this code,
				 * it moves by one. So when i=0, we move to position 1. Therefore at the last
				 * iteration, when i=index-1, we will get to position index. The same
				 * applies for the usage of this construct in the functions bellow.
				 */
			}
			}
		return currNode.getCon();
	}
	public void add(int data, int index)	{
		if (index==0)	{
			//if index is 0 we add at the beginning so we have to change the head
			//i.e. it's a special case
			head = new Node (data, head);
		}
		else	{
			Node currNode = head;
			Node addMe = new Node (data);
			//Gets the position of the current node & prepares the new node to be added
				
			for (int i=0; i<index; i++)	{
				currNode=currNode.getNext();
				//gets us to the position where we want to add
			}
			addMe.setNext(currNode.getNext());
			//makes our new Node link where it should
			currNode.setNext(addMe);
			//adds the new node at the appropriate position.
		}
		size++;
		//and the size of our array has grown by one
	}
	
	public void remove (int index)	{
		Node currNode = head;
		Node prevNode = null;
		//initializing variables
		if (index==0)	{
			//if deleting the first element of a list, the second becomes the new head.
			head = head.getNext();
		}
		else	{
			for (int i = 0; i<index; i++)	{
				prevNode=currNode;
				currNode=currNode.getNext();
				//getting currNode to equal the node we want to delete a prev note the one before
			}
			prevNode.setNext(currNode.getNext());
			//now prevNode will link directly to the node after currNode, which is therefore deleted
		}
	size--;
	//our list is now 1 element smaller
	}
	
	/*
	 * Question 8 -  Write a method to detect cycles in your linked list.  
	 * Theory: There can be at most one cycle in a single linked list, this occurs when the last element is linked to one
	 * of the previous elements rather then to null. Since we are keeping the size of our list, the way to approach this is
	 * to traverse our list and see whether we're going to traverse through more elements than the size of a list. If so,
	 * we have a cycle.
	 * */
	public boolean isCycle ()	{
		Node currNode = head;
		int traversed = 0;
		while (!(currNode==null))	{
			traversed++;
			if (traversed>size)	{
				return true;
		}
		}
		return false;	
	}
	
	/*Q9 - Describe the stack data structure
	 * The stack data structure is essentially a single-linked list where we only add to the top and remove from the top.
	 * It is implemented in the class StackDS which is a separate file. */

}
