package sVW1;

public class StackDS {
	private SLList myStack = new SLList ();
	//A stack is like a linked list
	public void add (int element)	{
		myStack.add(element, 0);
	}
	//except it can only add to index 0
	public void remove ()	{
		myStack.remove(0);
	}
	//and remove from index 0
	
	public void read ()	{
	//reads the first element of the stack
		myStack.getElement(0);
	}
	public void getSize ()	{
	//gets the stack size
		myStack.getSize();
	}
	
}
